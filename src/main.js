import Vue from "vue";
import VueCompositionAPI from "@vue/composition-api";
import App from "./App.vue";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faChevronRight,
  faChevronLeft,
  faCircle
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faChevronRight, faChevronLeft, faCircle);

Vue.component("FaIcon", FontAwesomeIcon);

Vue.use(VueCompositionAPI);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
